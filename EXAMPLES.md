
Execution with automatic copying of challenges to webroot
---------------------------------------------------------

```
> ./bacme -n -w /opt/share/www/ "example.com"
#### Using staging API
#### Creating new 384-bit ECDSA key
#### Creating domain subdirectory ...
#### Done. example.com/ created.
#### Getting URL of current subscriber agreement ...
#### OK https://letsencrypt.org/documents/LE-SA-v1.3-September-21-2022.pdf
#### Private key: example.com/account.key
Generating RSA private key, 4096 bit long modulus (2 primes)
........................................................................++++
....++++
e is 65537 (0x010001)
#### Public key: example.com/account.pub
writing RSA key
#### OK
#### Registering account ...
#### OK
#### Generating domain private key ...
#### Private key: example.com/example.com.key
read EC key
writing EC key
#### OK
#### Creating order ...
#### OK
#### Getting authorization tokens ...
####  for example.com
#### OK
#### Doing HTTP validation
#### Copying challenge tokens to DocumentRoot /opt/share/www/ ...
#### Done
#### Responding to challenges ...
#### OK
#### Waiting for validation ...
####  check 1: status=ready
#### Validation successful.
#### Deleting challenge tokens in DocumentRoot /opt/share/www/ ...
#### Done
#### Creating CSR ...
#### Done
#### Finalizing order ...
#### Waiting for certificate ...
####  check 1: status=valid
#### Certificate is ready for download.
#### OK
#### Downloading certificate ...
#### Success! Certificate saved to: example.com/example.com.crt
#### Intermediate certificate(s) saved to: example.com/ca.crt
#### Finished.
```


Execution with manual copying of challenges to webroot
------------------------------------------------------

```
> ./bacme example.com www.example.com
#### Creating domain subdirectory ...
#### Done. example.com/ created.
#### Getting URL of current subscriber agreement ...
#### OK https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf
#### Generating account key ...
#### Private key: example.com/account.key
Generating RSA private key, 4096 bit long modulus
.................................................................................................................................++++
..................................................++++
e is 65537 (0x010001)
#### Public key: example.com/account.pub
writing RSA key
#### OK
#### Registering account ...
#### OK
#### Generating domain private key ...
#### Private key: example.com/example.com.key
Generating RSA private key, 4096 bit long modulus
..................................++++
....................++++
e is 65537 (0x010001)
#### Creating order ...
#### OK
#### Getting authorization tokens ...
####  for example.com
####  for www.example.com
#### OK
#### Doing HTTP validation
#### Execute in your DocumentRoot:


mkdir -p .well-known/acme-challenge
echo 'zNmMWVkZWUwYTg4YmFkYjNlOWZkYmY0NjE0OWYxNWQg.NGU3ZGMzM2NkM2FjZjNmOTUxYmRiZWY2NDllMmUzMjk' > .well-known/acme-challenge/zNmMWVkZWUwYTg4YmFkYjNlOWZkYmY0NjE0OWYxNWQg
echo 'YmU1Yzc4ZTk3YWMyYjg2OWYxYzIzYTBjNWJhMWI5ODQ.ATU3MDU3MWMzZjBiZmJkYjdmMDMwNGVjOGU3NjlkYjd' > .well-known/acme-challenge/YmU1Yzc4ZTk3YWMyYjg2OWYxYzIzYTBjNWJhMWI5ODQ


#### Press [Enter] when done.

#### Responding to challenges ...
#### OK
#### Waiting for validation ...
#### Done
#### Creating CSR ...
#### Done
#### Finalizing order ...
#### OK
#### Downloading certificate ...
#### Success! Certificate saved: example.com/example.com.crt
#### You can do now in your DocumentRoot:


rm -r .well-known


#### Finished.
```


