bacme
=====

Documentation
-------------

This is a "keep it simple" shell script for requesting a certificate from the
Let's Encrypt CA using the ACME protocol.

Simplifications for example are:

- supports ACMEv2 (RFC 8555) only, not the deprecated ACMEv1
- supports http validation only
- keys are tried to be reused but also may be regenerated every time
- supports 4096 bit RSA and 256 or 384 bit ECDSA keys

The script is intentionally made so by default it will not do anything on your
server by itself. There is no need that you have to run it directly on your
server (as root or otherwise). You keep control over the validation and
installation process.

The script is intended to be easy to understand but still allow the complete
automatic generation of a certificate.
It is also a working small example to learn the ACME protocol.


Let's Encrypt Subscriber Agreement
----------------------------------

By using this script you accept the Let's Encrypt Subscriber Agreement.
The latest version can be found at https://letsencrypt.org/repository/


Usage
-----

```

Usage: bacme [options...] <domain> [ <domain> ... ]
Options:
  -e, --email EMAIL         Your email if you want that Let's Encrypt can contact you
  -h, --help                This help
  -1, --prime256            Use 256-bit ECDSA private domain key instead of 4096-bit RSA default
  -2, --secp384             Use 384-bit ECDSA private domain key
  -t, --test                Use staging API of Let's Encrypt for testing the script
  -v, --verbose             Verbose mode, print additional debug output
  -w, --webroot DIRECTORY   Path to the DocumentRoot of your webserver. Can be a rsync
                            compatible remote location like www@myserver:/srv/www/htdocs/
                            or can be a local location like /srv/www/htdocs/.

The first domain parameter should be your main domain name with the subdomains following after it.

Example: ./bacme -e me@example.com -w /var/www/example/ example.com www.example.com

```

See EXAMPLES.md for sample executions and their output.


Useful links
------------

- ACME protocol: https://tools.ietf.org/html/rfc8555
- Other ACME clients: https://letsencrypt.org/docs/client-options/

